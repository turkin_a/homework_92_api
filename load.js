const express = require('express');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ImageSchema = new Schema({
  type: {
    type: String
  },
  image: {
    type: Array
  }
});

const Image = mongoose.model('Image', ImageSchema);

const router = express.Router();

const createRouter = () => {
  router.get('/', (req, res) => {
    Image.find({}, {_id: false})
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });

  return router;
};

module.exports = createRouter;