const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');
const load = require('./load');
const app = express();
const expressWs = require('express-ws')(app);

const port = 8000;

app.use(cors());
app.use(express.json());

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

const activeConnections = [];

db.once('open', () => {
  console.log('Mongoose connected!');

  app.use('/images', load());

  app.ws('/canvas', (ws, req) => {
    console.log('Client connected');
    activeConnections.push(ws);
    const activeConnectionIndex = activeConnections.length - 1;

    let username = '';

    ws.on('message', (msg) => {
      const decodedMessage = JSON.parse(msg);

      switch (decodedMessage.type) {
        case 'CREATE_MESSAGE':
          const message = JSON.stringify({
            type: 'NEW_MESSAGE',
            text: decodedMessage.text
          });

          db.collection('images').insertOne({image: decodedMessage.text});

          activeConnections.forEach(connection => {
            connection.send(message);
          });
          break;
        default:
          console.log('Unknown message type', decodedMessage.type);
      }
    });

    ws.on('close', (msg) => {
      console.log('Client disconnected');
      activeConnections.splice(activeConnectionIndex, 1);
    })
  });

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
});